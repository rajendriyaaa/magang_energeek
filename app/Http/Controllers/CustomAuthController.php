<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Hash;
use Session;
use App\models\User;
use Illuminate\Support\Facades\Auth;
class CustomAuthController extends Controller
{
    
    public function index()
    {
        return view('auth.register');
    }  
      
    public function customregister(Request $request)
    {
        // @dd($request->all());

        $request->validate([
            
            'name' => 'required',
            'date' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required|min:10|max:15',
            'job' => 'required',
            'skill' => 'required'

        ]);
   
        $user = User::create([
            
            'name' =>$request->get("name"),
            'date' =>$request->get("date"),
            'email' =>$request->get("email"),
            'phone' =>$request->get("phone"),
            'job' =>$request->get("job"),
            'skill' =>$request->get("skill")
        ]);

        if($user) {
            return response()->json([
                'success' => true,
                'message' => 'Register Berhasil!'
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'status' => 400,
                'message' => 'Register Gagal!'
            ], 400);
       
        }
   
    }

}
    
    
