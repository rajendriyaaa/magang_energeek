@extends('dashboard')
@section('content')
<main class="register-form">
    <div class="cotainer">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card">
                    <h2 class="card-header text-center">Register</h2>
                    <div class="card-body">
                        {{-- <form method="POST" action="{{ route('register.custom') }}"> --}}
                            @csrf   
                            
                            <div class="form-group mb-3">
                                <input type="text" placeholder="Nama"  name="name" id="name" value="{{ old('name') }}" class="form-control" required autofocus>
                            </div>

                            <div class="form-group mb-3">
                                <input type="text" placeholder="BirthDay" name="date" id="date" value="{{ old ('date') }}" class="form-control" required
                                    autofocus>
                                @if ($errors->has('date'))
                                <span class="text-danger">{{ $errors->first('date') }}</span>
                                @endif
                            </div>

                            <div class="form-group mb-3">
                                <input type="text" placeholder="Email" name="email" id="email" value="{{ old ('email') }}" class="form-control" required
                                    autofocus>
                                @if ($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>

                            <div class="form-group mb-3">
                                <input type="number" placeholder="Phone" name="phone" id="phone" value="{{ old('phone') }}" class="form-control" required>
                                @if ($errors->has('phone'))
                                <span class="text-danger">{{ $errors->first('phone' ) }}</span>
                                @endif
                                <div class="form-group mb-3">
                            </div>
                        <div class="form-group mb-3">
                             <select name="job" id="job" class="form-control"  required
                                autofocus>
                                <option value="" disabled selected hidden> Job</option>
                                <option value="FrontEnd">FrontEnd</option>
                                <option value="BackEnd">BackEnd</option>
                                <option value="UI/UX">UI/UX</option>
                                <option value="FullStack">FullStack</option>
                                <option value="Mobile">Mobile</option>
                            </select> 
                            @if ($errors->has('job'))
                             <span class="text-danger">{{ $errors->first('job') }}</span>
                            @endif
                        </div>
                            
                        <div class="form-group mb-3">
                            <select  name="skill" id="skill" class="form-control" required
                            autofocus>
                                <option value="" disabled selected hidden>Skill_Set</option>
                                <option value="Vue JS">Vue JS</option>
                                <option value="Laravel">Laravel</option>
                                <option value="Figma">Figma</option>
                                <option value="Flutter">Flutter</option>
                            </select> 
                             @if ($errors->has('skill'))
                                <span class="text-danger">{{ $errors->first('skill') }}</span>
                            @endif
                        </div>

                    </div>
                            <div class="d-grid mx-auto">
                                <button type="button" class="btn btn-uploads btn-danger btn-block">Upload</button>
                            </div>
                        {{-- </form> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript">
$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            
            'X-CSRF-TOKEN': $('input[name="_token"]').val()
        }
    });

    $(".btn-uploads").click(function(e){

        e.preventDefault();

        var name = $("input[name=name]").val();
        var date = $("input[name=date]").val();
        var email = $("input[name=email]").val();
        var phone = $("input[name=phone]").val();
        var job = $("select[name=job]").val();
        var skill = $("select[name=skill]").val ();

        $.ajax({

        type:'POST',
        url:"{{ route('register.custom') }}" ,
        data:{
                "name":name, 
                "date":date, 
                "email":email, 
                "phone":phone, 
                "job":job, 
                "skill":skill },

        success:function(data){
            alert(data.message);
            window.location.replace("http://127.0.0.1:8000/register");
        }, error: function(data) {
            if(data.status == 422) {
                var data_messages = "";
                var errors = data.responseJSON.errors;
                $.each(errors, function(key1, messages) {
                    $.each(messages, function(key2, message) {
                        console.log(message);
                        data_messages = data_messages + message + " ";
                    });
                });

                alert(data_messages);
            } else {
                alert(data.message);
            }
        }

        });

    });
})
</script>
@endsection