<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomAuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/
Route::get('/', function () {
    return view('welcome');
});

Route::get('dashboard', [CustomAuthController::class, 'dashboard']); 
Route::get('register', [CustomAuthController::class, 'index'])->name('register.index');
Route::post('custom-register', [CustomAuthController::class, 'customregister'])->name('register.custom'); 